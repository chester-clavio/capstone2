const CryptoJS = require('crypto-js')
const jwt =  require('jsonwebtoken')

module.exports.createToken=(result)=>{
    return jwt.sign({id:result.id,email:result.email,password:result.password,isAdmin:result.isAdmin},process.env.SECRETKEY,{expiresIn:'1h'})
}

module.exports.verifyToken=(req,res,next)=>{
    let rawToken = req.headers.authorization
    if(rawToken==undefined){
        res.status(401).send({status:'error',error:'Token Missing'})
    }else{
        let token = rawToken.slice(7)
        jwt.verify(token,process.env.SECRETKEY,(err,data)=>{
            if(err){
                res.json({status:"error",error:"Invalid Token"})
            }else{
                next()
            }
        })
    }
}

module.exports.checkAdmin=(req,res,next)=>{
    
     let sessionInfo = decode(req.headers.authorization)
     sessionInfo.isAdmin? next(): res.json({status:"error",error:"Invalid access, Admin required"})
}
module.exports.userOnlyAccess=(req,res,next)=>{
    
    let sessionInfo = decode(req.headers.authorization)
    !sessionInfo.isAdmin? next(): res.json({status:"error",error:"Invalid access, Not for admin Access"})
}

module.exports.decodeToken=bearerToken=>decode(bearerToken);


function decode(bearerToken){
    bearerToken = bearerToken.slice(7)
    let payload = jwt.verify(bearerToken,process.env.SECRETKEY)
    return payload
 
}