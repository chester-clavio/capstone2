const express = require('express')
const router = express.Router();
const {verifyToken,checkAdmin} = require('./../auth')
const{
    getAllProducts,
    createProduct,
    updateProduct,
    archiveProduct,
    unArchiveProduct,
    productInfo,
    productsInfos,
    getActiveProducts,
    updateProductBuyCount,
    top3Products,
    addTag
}= require('./../controllers/productControllers')

router.get('/',(req,res)=>getAllProducts(req,res))

router.get('/activeProducts',(req,res)=>getActiveProducts(req,res))

router.post('/create',verifyToken,checkAdmin,(req,res)=>createProduct(req,res))

router.patch('/:id/update',verifyToken,checkAdmin,(req,res)=>updateProduct(req,res))

router.patch('/archive',verifyToken,checkAdmin,(req,res)=>archiveProduct(req,res))

router.patch('/unArchive',verifyToken,checkAdmin,(req,res)=>unArchiveProduct(req,res))

router.post('/productsInfos',(req,res)=>productsInfos(req,res))

router.patch('/updateBuyCount',(req,res)=>updateProductBuyCount(req,res))

router.get('/topProds',(req,res)=>top3Products(req,res))

router.patch('/addTag/:id',(req,res)=>addTag(req,res))

router.get('/:id/productInfo',verifyToken,(req,res)=>productInfo(req,res))

module.exports = router;