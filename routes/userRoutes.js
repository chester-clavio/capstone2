const express = require('express')
const router = express.Router();
const{userRegister,userLogin,makeAdmin,userInfo}=require('./../controllers/userControllers')

const {checkAdmin, verifyToken} = require('./../auth')

router.post('/register',(req,res)=>userRegister(req,res))

router.post('/login',(req,res)=>userLogin(req,res))

router.put('/giveAdminAccess',verifyToken,checkAdmin,(req,res)=>makeAdmin(req,res))

router.post('/userInfo',verifyToken,(req,res)=>userInfo(req,res))

router.get('/checkToken',verifyToken,(req,res)=>res.send({status:'ok'}))

module.exports = router