const express = require('express')
const router = express.Router()
const {orderCheckOut,getAllOrders,getUserOrders,orderDetails}= require('./../controllers/orderControllers')
const{verifyToken,checkAdmin,userOnlyAccess}=require('./../auth')

router.post('/create',verifyToken,userOnlyAccess,(req,res)=>orderCheckOut(req,res))

router.get('/userOrders',verifyToken,(req,res)=>getUserOrders(req,res))

router.get('/allOrders',verifyToken,checkAdmin,(req,res)=>getAllOrders(req,res))

router.get('/:id',(req,res)=>orderDetails(req,res))

module.exports = router