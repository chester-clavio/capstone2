const Order = require('./../models/Order')
const {decodeToken} = require('./../auth')

module.exports.orderCheckOut=async(req,res)=>{
    let {totalAmount,products}=req.body
    let token = req.headers.authorization;
    let email = decodeToken(token).email 
    let newOrder = new Order({
        emailOfCX:email,
        totalAmount:totalAmount,
        products:products
    })
    try{
        newOrder.save().then(result=>{
            res.json({status:"ok",newOrder:result})
        })
    }catch(err){
        res.json({status:"error",error:err})
    }

}

module.exports.getUserOrders=async(req,res)=>{
let token = req.headers.authorization;
let email = decodeToken(token).email 

Order.find({emailOfCX:email}).then(result=>{
    if(result===null){
        res.json({status:"error",error:"No Orders"})
    }else{
    res.json(result)
    }
})


}

module.exports.getAllOrders=async(req,res)=>Order.find().then(result=>res.json(result))

module.exports.orderDetails=async(req,res)=>Order.findById(req.params.id).then(result=>res.json(result))

