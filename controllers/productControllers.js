const req = require('express/lib/request')
const Product = require('./../models/Product')


module.exports.getAllProducts=async(req,res)=>Product.find().then(result=>res.json(result))

module.exports.getActiveProducts=async(req,res)=>Product.find({isActive:true}).then(result=>res.json(result))

module.exports.createProduct=async(req,res)=>{
    let {name,description,price,imgurl}=req.body
    let newProduct = new Product({
        name:name,
        description:description,
        price:price,
        imgurl:imgurl
    })
    try{
        await newProduct.save().then(result=>res.json(result))
    }catch(err){
        res.json({status:"error",error:err})
    }
}

module.exports.updateProduct=async(req,res)=>{
    try{
        await Product.findByIdAndUpdate(req.params.id,{$set:req.body},{new:true})
        .then(result=>{
        res.send({status:"update successful",newValues:result})
    })
    }catch(err){
        res.send({status:"error",error:"product with name already exist"})
    }
}

module.exports.archiveProduct=async(req,res)=>Product.findByIdAndUpdate({_id:req.body.id},{$set:{isActive:false}},{new:true}).then(result=>res.json({status:'ok',newValue:result}))

module.exports.unArchiveProduct=async(req,res)=>Product.findByIdAndUpdate({_id:req.body.id},{$set:{isActive:true}},{new:true}).then(result=>res.json({status:'ok',newValue:result}))

module.exports.productInfo=async(req,res)=>{
        await Product.findById(req.params.id)
        .then(result=>{
            res.send({values:result})
        })
}

module.exports.productsInfos=async(req,res)=>{
    let prodIds= req.body.prods;
    let newProds =[]

    for(let x=0;x<=prodIds.length-1;x++){
        await Product.findById(prodIds[x])
        .then(result=>{
            newProds.push(result)
            if(x==prodIds.length-1)
            {
                res.json(newProds)
            }
        })
    }
}

module.exports.updateProductBuyCount=async(req,res)=>{
    let products = req.body.prods
    let count;
    await products.forEach(prod => {
        
        Product.findById(prod.id).then(result=>{
            count=result.buycount+prod.quantity
            return result
        }).then(result=>{
            console.log(count,result.id)
            Product.findByIdAndUpdate(result.id,{$set:{buycount:count}})
            .then(result=>result)
        })
    })
    res.send({status:'ok'})
}


module.exports.top3Products=async(req,res)=>{
    Product.find({isActive:true}).sort({buycount:-1}).limit(3).then(result=>res.send(result))
}

module.exports.addTag=async(req,res)=>{
    let tagToAdd = req.body.tag
    tagToAdd = tagToAdd.map(tag=>tag.toLowerCase())

    Product.findByIdAndUpdate(req.params.id,{$set:{tags:tagToAdd}},{new:true})
        .then(result=>
        res.send(result)
        )

}
