const User = require('./../models/User')
const CryptoJS = require('crypto-js')
const {createToken,decodeToken} = require('./../auth')

module.exports.userRegister= async(req,res)=>{
    let newUser = new User({
        email: req.body.email,
        password: CryptoJS.AES.encrypt(req.body.password,process.env.SECRETKEY).toString()
    })
    try{
        await newUser.save().then(result=>{
            res.json({status:'ok',newUser:result})
        })
    }catch(err){
        res.status(500).json({status:"error",error:"duplicate email"})
    }
}

module.exports.userLogin= async(req,res)=>{
    let {email,password} = req.body

    await User.findOne({email:email}).then(result=>{
        if(result ===null){
            res.json({status:'error',error:'email not registered'})
        }else{
            let bytes = CryptoJS.AES.decrypt(result.password,process.env.SECRETKEY)
            let decodedPw = bytes.toString(CryptoJS.enc.Utf8)
            if(password==decodedPw){
                res.json({token:createToken(result),email:email,isAdmin:result.isAdmin})
            }else{
                res.json({status:'error',error:"invalid password"})
            }
        }
    })

    
}

module.exports.makeAdmin=async(req,res)=>User.findOneAndUpdate({email:req.body.email},{$set:{isAdmin:true}},{new:true}).then(result=>{
    if(result===null){
        res.json({status:"error",error:"Email not registered"})
    }else{
        res.json({status:"ok",newValue:result})
    }
})

module.exports.userInfo=async(req,res)=>User.findOne(req.body).then(result=>res.send(result))