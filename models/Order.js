const mongoose = require('mongoose')

const Order = new mongoose.Schema({
	emailOfCX:{type:String,required:true},
	totalAmount:{type:Number,required:true},
	purchasedOn:{type:Date,default:new Date()},
	products:[{
		productId:{type:String,required:true},
		quantity:{type:Number,required:true,default:1}
	}]

})

const model = mongoose.model("orders",Order)

module.exports = model;