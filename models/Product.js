const mongoose = require('mongoose');

const Product = new mongoose.Schema({
	name:{type:String,required:true,unique:true},
	description:{type:String,default:"No Description"},
	price:{type:Number,required:true},
	imgurl:{type:String,default:"https://cdn-icons-png.flaticon.com/512/3724/3724788.png"},
	buycount:{type:Number,default:0},
	tags:[{type:String}],
	isActive:{type:Boolean,default:true},
	createdOn:{type:Date,default:new Date()}
})

const model = mongoose.model("products",Product)

module.exports = model;