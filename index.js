const dotenv = require('dotenv').config()
const port = process.env.PORT||3000;
const cors = require('cors')
const express = require('express');
const app = express();

app.use(cors());
app.use(express.json())

//Routers
const userRoute = require('./routes/userRoutes')
app.use('/api/users',userRoute)
const productRoute = require('./routes/productRoutes')
app.use('/api/products',productRoute)
const orderRoute = require('./routes/orderRoutes')
app.use('/api/orders',orderRoute)

//database connect
const mongoose = require('mongoose')
mongoose.connect(process.env.MONGO)
const db = mongoose.connection;
db.on('open',()=> console.log("Connected to database"))
db.on('error',console.error.bind(console,"database error"))

app.listen(port,()=>console.log("Server started on port",port))